﻿using System.Web;
using System.Web.Mvc;

namespace wersja_ostateczna_forum
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
