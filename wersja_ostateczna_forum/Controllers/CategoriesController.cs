﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using wersja_ostateczna_forum.Models;
using PagedList;
using PagedList.Mvc;
using Microsoft.AspNet.Identity;

namespace wersja_ostateczna_forum.Controllers
{
    public class CategoriesController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Categories/forum/5
        public ActionResult forum(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Item item = db.Items
            // .Include(i => i.Category)
            // .Include(i => i.Brand)
            // .SingleOrDefault(x => x.ItemId == id);


            //var threads = db.Threads.Include(t => t.Category).Include(t => t.Posts).ToList().s(x => x.CategoryID == id);
            var lista_watkow = db.Categories.Find(id).Threads.ToList().OrderByDescending(x => x.DateTime);
            ViewBag.kategoria = db.Categories.Find(id).Title;
            if (lista_watkow == null)
            {
                return HttpNotFound();
            }
            return View(lista_watkow.ToPagedList(page ?? 1, 10));
        }

        // GET: Categories/Create
        public ActionResult Create()
        {
            ViewBag.SectionID = new SelectList(db.Sections, "SectionID", "Title");
            return View();
        }
        public ActionResult create_topic(int CategoryID)
        {
            ViewBag.dzialaj = CategoryID;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult create_topic(Category_Topic_Post zbieranina)//otrzymuje tytul i content
        {
            ///najpierw stworzyc nowy watek
            var topic = new Thread();
            topic.CategoryID = zbieranina.CategoryID;
            topic.Owner = User.Identity.GetUserName();
            topic.Title = zbieranina.Title;
            topic.DateTime = DateTime.Now;
            db.Threads.Add(topic);
            db.SaveChanges();
            // sie udalo
            //teraz utworzyc post do watku
            var post = new Post();
            post.Content = zbieranina.Content;
            post.DateTime = topic.DateTime;
            post.UserID = User.Identity.GetUserId();
            post.ThreadID = topic.ThreadID;
            db.Posts.Add(post);
            db.SaveChanges();
            //powinno sie utworzyc




            //post.DateTime = DateTime.Now;
            //post.UserID = User.Identity.GetUserId();
            //if (ModelState.IsValid)
            //{
              //  db.Posts.Add(post);
               // db.SaveChanges();
              //  return RedirectToAction("topic/1");
            //}
            return View();
        }
        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CategoryID,Title,SectionID")] Category category)
        {
            if (ModelState.IsValid)
            {
                db.Categories.Add(category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SectionID = new SelectList(db.Sections, "SectionID", "Title", category.SectionID);
            return View(category);
        }

        // GET: Categories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            ViewBag.SectionID = new SelectList(db.Sections, "SectionID", "Title", category.SectionID);
            return View(category);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CategoryID,Title,SectionID")] Category category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SectionID = new SelectList(db.Sections, "SectionID", "Title", category.SectionID);
            return View(category);
        }

        // GET: Categories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Category category = db.Categories.Find(id);
            db.Categories.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public class Formatter : IFormatProvider, ICustomFormatter
        {
            public object GetFormat(Type formatType)
            {
                if (formatType == typeof(ICustomFormatter))
                    return this;

                return null;
            }

            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                if (!(arg is DateTime)) throw new NotSupportedException();

                var dt = (DateTime)arg;

                string suffix;

                if (new[] { 11, 12, 13 }.Contains(dt.Day))
                {
                    suffix = "th";
                }
                else if (dt.Day % 10 == 1)
                {
                    suffix = "st";
                }
                else if (dt.Day % 10 == 2)
                {
                    suffix = "nd";
                }
                else if (dt.Day % 10 == 3)
                {
                    suffix = "rd";
                }
                else
                {
                    suffix = "th";
                }

                return string.Format("{0:MMMM} {1}{2}, {0:yyyy}", arg, dt.Day, suffix);
            }
        }
    }
}
