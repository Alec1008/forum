﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Collections;
using System;
using wersja_ostateczna_forum.Migrations;

namespace wersja_ostateczna_forum.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        public bool Active { get; set; }
        public string Avatar { get; set; }
        public int Reputation { get; set; }
        public bool NickChanged { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public int RankID { get; set; }
        public virtual Rank Rank { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public DateTime LastLogin { get; set; }
        public string Nick { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("wersja_ostateczna", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Annoucement> Annoucements { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Emoticone> Emoticones { get; set; }
        public DbSet<ForbiddenWord> ForbiddenWords { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Moderator> Moderators  {get;set;}
        public DbSet<Post> Posts { get; set; }
        public DbSet<Rank> Ranks { get; set; }
        public DbSet<Thread> Threads { get; set; }
        public DbSet<Section> Sections { get; set; }
    }
}