﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wersja_ostateczna_forum.Models
{
    public class Moderator
    {
        public int ModeratorID { get; set; }
        public string UserID { get; set; }
        public int CategoryID { get; set; }
        public virtual Category Category { get; set; }
    }
}