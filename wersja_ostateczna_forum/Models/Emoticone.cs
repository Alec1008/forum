﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wersja_ostateczna_forum.Models
{
    public class Emoticone
    {
        public int EmoticoneID { get; set; }
        public string Pattern { get; set; }
        public string Path { get; set; }
    }
}