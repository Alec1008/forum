﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wersja_ostateczna_forum.Models
{
    public class Category
    {
        public int CategoryID { get; set; }
        public string Title { get; set; }
        public virtual ICollection<Moderator> Moderators { get; set; }
        public virtual ICollection<Thread> Threads { get; set; }
        public int SectionID { get; set; }
        public virtual Section Section { get; set; }
        public string Description { get; set; }

    }
}