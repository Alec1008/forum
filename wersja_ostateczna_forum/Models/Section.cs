﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wersja_ostateczna_forum.Models
{
    public class Section
    {
        public int SectionID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public virtual  ICollection<Category> Categories { get; set; }
    }
}