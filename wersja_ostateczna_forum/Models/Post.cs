﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace wersja_ostateczna_forum.Models
{
    public class Post
    {
        public int PostID { get; set; }
        public string Content { get; set; }

        [DisplayFormat(DataFormatString = "Answered on {0:HH:mm}, {0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Time)]
        public DateTime DateTime { get; set; }
        public int ThreadID { get; set; }
        public Thread Thread { get; set; }
        public string UserID { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}