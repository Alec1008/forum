﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wersja_ostateczna_forum.Models
{
    public class Image
    {
        public int ImageID { get; set; }
        public string Path { get; set; }
    }
}