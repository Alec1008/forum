﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wersja_ostateczna_forum.Models
{
    public class Rank
    {
        public int RankID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }

    }
}