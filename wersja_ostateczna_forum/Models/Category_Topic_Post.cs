﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace wersja_ostateczna_forum.Models
{
    public class Category_Topic_Post
    {
        public int CategoryID { get; set; } // jest id kategorii ********chyba jest********

        /////////
        public string Content { get; set; } // zawartosc nowego posta tworzonwego w nowym wątku *******jest*************

        [DisplayFormat(DataFormatString = "Answered on {0:HH:mm}, {0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Time)]
        public DateTime DateTime { get; set; } // data utworzenia nowego watku z postem**************np***********
        public string UserID { get; set; } // wyciagnac userID************np***********
        ///////////
        public string Title { get; set; } // tytul watku***************np***********
        public string Owner { get; set; } // dac tu username usera************np**********
    }
}