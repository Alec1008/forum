﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wersja_ostateczna_forum.Models
{
    public class Annoucement
    {
        public int AnnoucementID { get; set; }
        public string Content { get; set; }
        public string Title { get; set; }
        public bool Active { get; set; }
        public string Owner { get; set; }
    }
}