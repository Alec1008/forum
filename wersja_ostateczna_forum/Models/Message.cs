﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wersja_ostateczna_forum.Models
{
    public class Message
    {
        public int MessageID { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string ReceivedBy { get; set; }
        public DateTime DateTime { get; set; }
        public string UserID { get; set; }
        public virtual ApplicationUser User { get; set; }
        public bool Read { get; set; }

    }
}