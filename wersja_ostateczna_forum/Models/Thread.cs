﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace wersja_ostateczna_forum.Models
{
    public class Thread
    {
        public int ThreadID { get; set; }
        public string Title { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Time)]
        public DateTime DateTime { get; set; }
        public int CategoryID { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public string Owner { get; set; }
        public bool Closed { get; set; }
    }
}