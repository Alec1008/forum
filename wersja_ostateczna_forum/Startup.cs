﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(wersja_ostateczna_forum.Startup))]
namespace wersja_ostateczna_forum
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
